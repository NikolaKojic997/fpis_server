import { StavkaOtpremnice } from "src/stavka-otpremnice/stavka-otpremnice.entity";
import { Column, Entity, OneToMany, PrimaryColumn } from "typeorm";
import {IsDate, IsInt, IsNotEmpty, isNumber, IsNumber, IsString, ValidateNested } from 'class-validator'
import {Type} from 'class-transformer'

@Entity()
export class Otpremnica{
    @PrimaryColumn({name: 'sifra_otpremnice'})
    @IsInt({message: 'Otpremnica mora da bude integer tipa'})
    SifraOtpremnice : number
    @Column({nullable:true})
    @IsNotEmpty({message: "Molimo unesite ime skladista."})
    skladiste : string
    @Column({nullable:true})
    @IsNotEmpty({message: "Molimo unesite ime maloprodaje."})
    maloprodaja : string
    @Column({nullable:true})
    datumDokumenta : Date
    @OneToMany(() => StavkaOtpremnice, (stavka) => stavka.otpremnica, {onUpdate: 'CASCADE'})
    @ValidateNested({ each: true })
    @Type(() => StavkaOtpremnice)
    stavke: StavkaOtpremnice[];
    
}