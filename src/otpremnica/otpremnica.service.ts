import { Injectable } from '@nestjs/common';
import { Repository, Connection, QueryRunner } from 'typeorm';
import { Otpremnica } from './otpremnica.entity';
import { InjectRepository, InjectConnection, } from "@nestjs/typeorm";
import { HttpStatus, HttpException } from '@nestjs/common'
import { StavkaOtpremnice } from 'src/stavka-otpremnice/stavka-otpremnice.entity';


@Injectable()
export class OtpremnicaService {
    private qr: QueryRunner;
    private otpremnicaRepo: any;
    private stavkaRepo: any;
    constructor(
        @InjectConnection()
        private readonly connection: Connection
    ) {
        this.qr = this.connection.createQueryRunner();
        this.otpremnicaRepo = this.qr.manager.getRepository(Otpremnica);
        this.stavkaRepo = this.qr.manager.getRepository(StavkaOtpremnice);
    }
    async deleteOne(id: number) {
        try {
            this.qr.startTransaction();
            const res = await this.otpremnicaRepo.delete(id);
            if (res.raw.affectedRows === 0) {
                throw new HttpException(
                    "Otpremnica with given id not found",
                    HttpStatus.BAD_REQUEST
                )
            }
            this.qr.commitTransaction();
            return true;
        }
        catch (e) {
            this.qr.rollbackTransaction();
            throw new HttpException(
                e.message,
                HttpStatus.BAD_REQUEST
            )
        }
    }
    async save(o: Otpremnica): Promise<any> {

        let subset = (({ SifraOtpremnice, skladiste, maloprodaja, datumDokumenta}) => ({ SifraOtpremnice, skladiste, maloprodaja, datumDokumenta}))(o)
        try {
            this.qr.startTransaction();
            if (await this.otpremnicaRepo.findOne(o.SifraOtpremnice)) {
                await this.otpremnicaRepo.update(o.SifraOtpremnice, subset);
            }
            else {
                await this.otpremnicaRepo.save(o);
            }
            for (let s of o.stavke) {
                s.sifra_otpremnice = o.SifraOtpremnice;
                switch (s.operation) {
                    case ('D'): {
                        s.operation = ''
                        await this.stavkaRepo.delete({ sifra_otpremnice: s.sifra_otpremnice, redni_broj: s.redni_broj });
                        break;
                    }
                    case ('I'): {
                        s.operation = ''
                        await this.stavkaRepo.save(s);
                        break;
                    }
                    case ('U'): {
                        s.operation = ''
                        await this.stavkaRepo.update({ sifra_otpremnice: s.sifra_otpremnice, redni_broj: s.redni_broj }, s);
                        break;
                    }
                }
            }
            this.qr.commitTransaction();
            return true;
        }
        catch (e) {
            this.qr.rollbackTransaction();
            throw new HttpException(
                e.message,
                HttpStatus.BAD_REQUEST
            );
        }
    }
    async getOne(id: number): Promise<Otpremnica> {
        const o = await this.otpremnicaRepo.findOne(id, { relations: ["stavke", "stavke.pdv", "stavke.proizvod", "stavke.proizvod.jedinica_mere"] });
        if (!o) {
            throw new HttpException(
                "otpremnica with given id not found",
                HttpStatus.BAD_REQUEST
            )
        }
        return o;
    }
    async getAll() {
        return this.otpremnicaRepo.find({ relations: ["stavke", "stavke.pdv", "stavke.proizvod", "stavke.proizvod.jedinica_mere"] });
    }
}
