import { Body, Controller,Put, Get,Post, Param, Delete, UseGuards, UsePipes, ValidationPipe } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { LocalAuthGuard } from 'src/auth/guards/local-auth.guard';
import { Otpremnica } from './otpremnica.entity';
import { OtpremnicaService } from './otpremnica.service';

@Controller('otpremnica')
@UsePipes(new ValidationPipe())
export class OtpremnicaController {
    constructor(private readonly otpremnicaService: OtpremnicaService){}

    @Get()
    @UseGuards(JwtAuthGuard)
    getAll(){
        return this.otpremnicaService.getAll();
    }

    @Get(':id')
    getOne(@Param('id') id: number){
        return this.otpremnicaService.getOne(id);
    }

    @Post()
    @UsePipes(new ValidationPipe())
    insert(@Body() o : Otpremnica){
        return this.otpremnicaService.save(o);
    }

    @Put()
    Update(@Body() o : Otpremnica){
        return this.otpremnicaService.save(o);
    }
    @Delete(':id')
    DeleteOne(@Param('id') id : number){
        return this.otpremnicaService.deleteOne(id);
    }
}
