import { Test, TestingModule } from '@nestjs/testing';
import { OtpremnicaService } from './otpremnica.service';

describe('OtpremnicaService', () => {
  let service: OtpremnicaService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [OtpremnicaService],
    }).compile();

    service = module.get<OtpremnicaService>(OtpremnicaService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
