import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { StavkaOtpremniceModule } from 'src/stavka-otpremnice/stavka-otpremnice.module';
import { StavkaOtpremniceService } from 'src/stavka-otpremnice/stavka-otpremnice.service';
import { OtpremnicaController } from './otpremnica.controller';
import { Otpremnica } from './otpremnica.entity';
import { OtpremnicaService } from './otpremnica.service';



@Module({
  imports: [TypeOrmModule.forFeature([Otpremnica]),StavkaOtpremniceModule],
  controllers: [OtpremnicaController],
  providers: [OtpremnicaService],
  exports: [OtpremnicaService]
})
export class OtpremnicaModule {}
