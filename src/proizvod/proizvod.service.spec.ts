import { Test, TestingModule } from '@nestjs/testing';
import { ProizvodService } from './proizvod.service';

describe('ProizvodService', () => {
  let service: ProizvodService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ProizvodService],
    }).compile();

    service = module.get<ProizvodService>(ProizvodService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
