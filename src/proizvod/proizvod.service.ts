import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm'
import { Proizvod } from 'src/entities/proizvod.entity';
import { Repository } from 'typeorm';


@Injectable()
export class ProizvodService {
    constructor(
        @InjectRepository(Proizvod)
        private readonly proizvodRepo: Repository<Proizvod>) { }
    
    async getAll() {
        return await this.proizvodRepo.find({relations : ['jedinica_mere']})
    }
}
