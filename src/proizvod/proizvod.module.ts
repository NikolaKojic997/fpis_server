import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Proizvod } from 'src/entities/proizvod.entity';
import { StavkaOtpremniceModule } from 'src/stavka-otpremnice/stavka-otpremnice.module';
import { StavkaOtpremniceService } from 'src/stavka-otpremnice/stavka-otpremnice.service';
import { ProizvodController } from './proizvod.controller';
import { ProizvodService } from './proizvod.service';

@Module({
  imports: [TypeOrmModule.forFeature([Proizvod])],
  controllers: [ProizvodController],
  providers: [ProizvodService],
  exports: [ProizvodService]
})
export class ProizvodModule {}
