import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm'
import { StavkaOtpremniceModule } from './stavka-otpremnice.module';
import { HttpException, HttpStatus } from '@nestjs/common'
import { StavkaOtpremnice } from './stavka-otpremnice.entity';

@Injectable()
export class StavkaOtpremniceService {
    constructor(
        @InjectRepository(StavkaOtpremnice)
        private readonly stavkaRepo: Repository<StavkaOtpremniceModule>) { }
    
    async insert(s: StavkaOtpremniceService): Promise<any> {
        try {
            return await this.stavkaRepo.save(s);
        }
        catch (e) {
            throw new HttpException(
                e.message,
                HttpStatus.BAD_REQUEST
            );
        }
    }
}
