import { Test, TestingModule } from '@nestjs/testing';
import { StavkaOtpremniceService } from './stavka-otpremnice.service';

describe('StavkaOtpremniceService', () => {
  let service: StavkaOtpremniceService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [StavkaOtpremniceService],
    }).compile();

    service = module.get<StavkaOtpremniceService>(StavkaOtpremniceService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
