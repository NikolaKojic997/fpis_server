import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { StavkaOtpremniceController } from './stavka-otpremnice.controller';
import {StavkaOtpremnice } from './stavka-otpremnice.entity';
import { StavkaOtpremniceService } from './stavka-otpremnice.service';

@Module({
  imports: [TypeOrmModule.forFeature([StavkaOtpremnice])],
  controllers: [StavkaOtpremniceController],
  providers: [StavkaOtpremniceService],
  exports: [StavkaOtpremniceService]
})
export class StavkaOtpremniceModule {}
