import { PDV } from "src/entities/pdv.entity";
import { Proizvod } from "src/entities/proizvod.entity";
import { Otpremnica } from "src/otpremnica/otpremnica.entity";
import { Column, Entity, JoinColumn, ManyToOne } from "typeorm";
import { IsInt, IsNumber, Max, Min } from 'class-validator'

@Entity({ name: 'stavka_otpremnice' })
export class StavkaOtpremnice {
    @Column("int", { primary: true, name: "sifra_otpremnice" })
    sifra_otpremnice: number;

    @Column("int", { primary: true, name: "redni_broj" })
    @IsInt({message: 'Redni Broj mora biti integer tipa.'})
    redni_broj: number;

    @ManyToOne(() => Otpremnica, (o) => o.stavke, { onDelete: 'CASCADE', onUpdate: 'CASCADE'})
    @JoinColumn({ name: "sifra_otpremnice" })
    otpremnica: Otpremnica;
    @Column()
    @IsInt({message: 'Rabat mora biti integer tipa.'})
    @Min(0, {message: 'Rabat mora biti broj u opsegu od 0 do 100'})
    @Max(100, {message: 'Rabat mora biti broj u opsegu od 0 do 100'} )
    rabat: number
    @Column()
    @IsInt({message: 'Kolicina mora biti integer tipa.'})
    @Min(0, {message: 'Kolicina mora biti pozitivan broj.'})
    kolicina: number
    @ManyToOne(() => Proizvod, (p) => p.stavke, {})
    @JoinColumn({ name: "sifra_proizvoda" })
    proizvod: Proizvod
    @ManyToOne(() => PDV, (pdv) => pdv.stavke, {})
    @JoinColumn({ name: "sifra_pdv" })
    pdv: PDV
    @Column({ nullable: true })
    operation: string
}





