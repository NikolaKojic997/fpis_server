import { Test, TestingModule } from '@nestjs/testing';
import { StavkaOtpremniceController } from './stavka-otpremnice.controller';

describe('StavkaOtpremniceController', () => {
  let controller: StavkaOtpremniceController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [StavkaOtpremniceController],
    }).compile();

    controller = module.get<StavkaOtpremniceController>(StavkaOtpremniceController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
