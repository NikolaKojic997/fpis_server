import { Controller, UsePipes,ValidationPipe} from '@nestjs/common';

@Controller('stavka-otpremnice')
@UsePipes(new ValidationPipe())
export class StavkaOtpremniceController {}
