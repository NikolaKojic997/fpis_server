import { ExtractJwt, Strategy, JwtPayload } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { AuthService } from '../auth.service';
import {UnauthorizedException} from '@nestjs/common'

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly authService: AuthService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: "mySecret",
    });
    
  }

  async validate(payload: JwtPayload):Promise<any> {
    const {id} = payload
    
    let  user = this.authService.getUser(id);
    if(!user) {
      throw new UnauthorizedException()
    }
    return payload;
  }
}