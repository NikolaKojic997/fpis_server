import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UserService } from 'src/user/user.service';

@Injectable()
export class AuthService {
  constructor( private readonly userService: UserService) {}
  getUser(id: any): any {
    return this.userService.getOne(id);
  }
}
