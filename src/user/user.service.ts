import { Injectable, HttpStatus, HttpException } from '@nestjs/common';
import { User } from './user.entity';
import { InjectRepository } from '@nestjs/typeorm'
import { Repository } from 'typeorm'

@Injectable()
export class UserService {
    constructor(@InjectRepository(User)
    private readonly userRepo: Repository<User>) { }
    insert(u: User) {
        return this.userRepo.insert(u);
    }
    async getOne(id: string) {
        let u = await this.userRepo.findOne(id);
        if (!u)
            throw new HttpException(
                'Nema trazenog usera',
                HttpStatus.BAD_REQUEST
            );
        return u;

    }
}
