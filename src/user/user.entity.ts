import {Column, Entity, PrimaryColumn} from 'typeorm'

@Entity({name:'user'})
export class User {
    @PrimaryColumn()
    userID: string
    @Column()
    fullName: string
}