import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { User } from './user.entity';
import { UserService } from './user.service';

@Controller('user')
export class UserController {
    constructor(private readonly userService: UserService){}
    @Get(':id')
    getOne(@Param('id') id :string){
        return this.userService.getOne(id);
    }
    @Post()
    insert(@Body() u :User){
        return this.userService.insert(u);
    }
}
