import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OtpremnicaModule } from './otpremnica/otpremnica.module';
import { StavkaOtpremniceModule } from './stavka-otpremnice/stavka-otpremnice.module';
import { ProizvodService } from './proizvod/proizvod.service';
import { ProizvodController } from './proizvod/proizvod.controller';
import { ProizvodModule } from './proizvod/proizvod.module';
import { PdvController } from './pdv/pdv.controller';
import { PdvService } from './pdv/pdv.service';
import { PdvModule } from './pdv/pdv.module';
import { AuthService } from './auth/auth.service';
import { AuthModule } from './auth/auth.module';
import { UserController } from './user/user.controller';
import { UserService } from './user/user.service';
import { UserModule } from './user/user.module';


@Module({
  imports: [TypeOrmModule.forRoot(),PdvModule, OtpremnicaModule,UserModule, StavkaOtpremniceModule, ProizvodModule, AuthModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
