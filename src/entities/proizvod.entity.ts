import {StavkaOtpremnice } from 'src/stavka-otpremnice/stavka-otpremnice.entity'
import {Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryColumn} from 'typeorm'
import { jedinicaMere } from './jedinica-mere.entity'

@Entity({name:'proizvod'})
export class Proizvod {
    @PrimaryColumn()
    sifraProizvoda: number
    @Column()
    cena: number
    @Column()
    naziv: string
    @ManyToOne(() => jedinicaMere, (jm) => jm.proizvodi, {})
    @JoinColumn({ name: "sifra_jm"})
    jedinica_mere: jedinicaMere
    @OneToMany(() => StavkaOtpremnice, (s) => s.proizvod, {})
    stavke: StavkaOtpremnice[];
}