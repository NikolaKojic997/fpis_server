import {Column, Entity, PrimaryColumn} from 'typeorm'

@Entity({name:'pdv'})
export class PDV {
    @PrimaryColumn()
    sifraPDV: number
    @Column()
    iznosPDV: number
    stavke: PDV[]
}