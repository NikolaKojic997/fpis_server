import {Column, Entity, OneToMany, PrimaryColumn} from 'typeorm'
import { Proizvod } from './proizvod.entity'

@Entity({name:'jedinica_mere'})
export class jedinicaMere {
    @PrimaryColumn()
    sifraJediniceMere: number
    @Column()
    naziv: string
    proizvodi: Proizvod[]
}