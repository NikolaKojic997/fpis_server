import { Injectable } from '@nestjs/common';
import { PDV } from 'src/entities/pdv.entity';
import { InjectRepository } from '@nestjs/typeorm'
import {Repository} from 'typeorm'

@Injectable()
export class PdvService {
    constructor(
        @InjectRepository(PDV)
        private readonly pdvRepo: Repository<PDV>
    ){}
    async getAll() {
        return await this.pdvRepo.find();
    }
}
