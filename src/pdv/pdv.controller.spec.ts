import { Test, TestingModule } from '@nestjs/testing';
import { PdvController } from './pdv.controller';

describe('PdvController', () => {
  let controller: PdvController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PdvController],
    }).compile();

    controller = module.get<PdvController>(PdvController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
