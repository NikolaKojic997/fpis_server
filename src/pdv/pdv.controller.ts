import { Controller, Get } from '@nestjs/common';
import { PdvService } from './pdv.service';

@Controller('pdv')
export class PdvController {
    constructor(private readonly pdvService: PdvService){

    }

    @Get()
    async GetAll(){
        return await this.pdvService.getAll();
    }

}
