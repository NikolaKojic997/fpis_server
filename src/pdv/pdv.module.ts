import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PDV } from 'src/entities/pdv.entity';
import { Proizvod } from 'src/entities/proizvod.entity';
import { PdvController } from './pdv.controller';
import { PdvService } from './pdv.service';


@Module({
  imports: [TypeOrmModule.forFeature([PDV])],
  controllers: [PdvController],
  providers: [PdvService],
  exports: [PdvService]
})
export class PdvModule {}
